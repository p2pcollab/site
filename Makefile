SOURCES := $(shell git ls-files --recurse-submodules)
TARGETS := $(patsubst %.org,%.html,$(SOURCES))
OUT := _build/_html

STYLES := \
	pub/tufte-css/tufte.css \
	pub/tufte-pandoc-css/pandoc.css \
	pub/tufte-pandoc-css/pandoc-solarized.css \
	pub/tufte-pandoc-css/tufte-extra.css \
	style/style.css

TMPL := pub/tufte.html5

space :=
space +=

.PHONY: all
all: $(TARGETS) src doc

%.html: %.org $(TMPL) $(STYLES) style
	mkdir -p $(dir $(OUT)/$@)
	pandoc \
		--toc \
		--section-divs \
		--katex \
		--from org+tex_math_single_backslash \
		--filter pandoc-sidenote \
		--to html5+smart \
		--template=$(TMPL) \
		$(foreach style,$(STYLES),--css $(subst $(space),,$(patsubst %,../,$(subst /, ,$(subst ./,,$(dir $@)))))style/$(notdir $(style))) \
		--output $(OUT)/$@ \
		$<

.PHONY: clean
clean:
	rm -rf out

.PHONY: style
style:
	mkdir -p $(OUT)/style
	test -L $(OUT)/img || ln -sf ../../img $(OUT)/img
	cp -a \
		$(STYLES) \
		pub/tufte-css/latex.css \
		pub/tufte-css/et-book/ \
		$(OUT)/style


.PHONY: doc
doc:
	dune external-lib-deps --missing @doc 2>&1 | grep -o 'opam install .*' | sed 's/\(opam install\)/\1 -y/' | sh
	dune build @doc
	rm -rf $(OUT)/doc/ocaml || true
	mkdir -p $(OUT)/doc
	mv _build/default/_doc/_html $(OUT)/doc/ocaml

.PHONY: src
src:
	git submodule update --init --remote
